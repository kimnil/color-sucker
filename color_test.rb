#!/usr/bin/env ruby
require 'rubygems'
require 'RMagick'
include Magick

# Settings
NUM_COLORS = 10
HIST_HEIGHT = 200
BAR_WIDTH = 30
COLORSPACES = [Magick::ColorspaceType::YUVColorspace,
                Magick::ColorspaceType::YIQColorspace,
                Magick::ColorspaceType::RGBColorspace]

def create_histogram(image_uri, colorspace) 
  begin_timer = Time.now
  begin
    img = Image.read(image_uri).first
    print "working on #{image_uri} (#{img.columns}x#{img.rows}) (#{colorspace})"
  rescue
    puts 'Couldn\'t find file: ' + image_uri
    exit
  end
  img = img.quantize(NUM_COLORS, colorspace)

  hist = img.color_histogram

  begin
    pixels = hist.keys.sort_by {|pixel| hist[pixel] }
  rescue NameError    # No sort_by?
    pixels = hist.keys.sort { |p, q| hist[p] <=> hist[q] }
  end

  # calculations are done now
  done_timer = Time.now

  scale = HIST_HEIGHT / (hist.values.max*1.025)   # put 2.5% air at the top

  gc = Draw.new
  gc.stroke_width(BAR_WIDTH)
  gc.affine(1, 0, 0, -scale, 0, HIST_HEIGHT)

  # handle images with fewer than NUM_COLORS colors
  start = NUM_COLORS - img.number_colors

  pixels.each { |pixel|
    gc.fill(pixel.to_color)
    gc.rectangle(start*BAR_WIDTH, 0, start*BAR_WIDTH+BAR_WIDTH, hist[pixel])
    start = start.succ
  }

  hatch = HatchFill.new("white", "gray95")
  canvas = Image.new(NUM_COLORS*BAR_WIDTH, HIST_HEIGHT, hatch)
  gc.draw(canvas)

  filename = image_uri.scan(/[\w,\.-]+\.\w+$/)
  text = Draw.new
  text.annotate(canvas, 0, 0, 0, 20, "#{filename}\nHistogram") {
    self.pointsize = 10
    self.gravity = NorthGravity
    self.stroke = 'transparent'
  }

  canvas.border!(1, 1, "white")
  canvas.border!(1, 1, "black")
  canvas.border!(3, 3, "white")
  canvas.write("histograms/#{filename}_#{colorspace}_histogram.gif")

  puts "... done! took #{done_timer - begin_timer} seconds"
end


if ARGV.length != 1
  puts 'Usage: [<input folder/> || <input file>]'
  exit
else
  if ARGV[0].end_with?('/')
    begin
      Dir.foreach(ARGV[0]) do |file|
        next if file.start_with? '.' # hidden files, . and ..
        COLORSPACES.each do |cs|
          create_histogram(ARGV[0] + file, cs)
        end
      end
    rescue 
      puts "That\'s not a folder in your file system, silly you!"
      exit
    end
    exit
  else
    image_uri = ARGV[0]
    COLORSPACES.each do |cs|
      create_histogram(image_uri, cs)
    end
    exit
  end
end
